'use strict';

var React = require('react');

var CheckMessage = React.createClass({
  render: function () {
    var type = this.props.type;
    var msgn = this.props.message;

    var className = 'alert alert-'.concat(type);

    return React.createElement(
      'div',
      { className: className },
      msgn
    );
  }
});

module.exports = function (message, ref, type) {
  if (!type) type = 'danger';
  if (!message) message = '';

  return React.createElement(CheckMessage, {
    type: type,
    message: message
  });
};