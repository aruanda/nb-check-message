'use strict';

var gulp = require('gulp');
var babel = require('gulp-babel');

gulp.task('build', function() {
  var babelOptions = {
    plugins: [
      ['transform-react-jsx']
    ]
  };

  return gulp
    .src(['nb-check-message.jsx'])
    .pipe(babel(babelOptions))
    .pipe(gulp.dest('./'));
});
